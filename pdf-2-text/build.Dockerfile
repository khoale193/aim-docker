FROM python:3.9-buster

ENV APP_HOME /app

RUN mkdir $APP_HOME

WORKDIR $APP_HOME

CMD echo BUILDING... \
  && cd pdf-to-text/ \
  && pip3 install -r requirements.txt \
  && apt-get update \
  && apt-get install ffmpeg libsm6 libxext6  -y \
  && apt-get update \
  && apt-get --fix-missing update \
  && apt-get --fix-broken install \
  && apt-get install -y poppler-utils \
  && apt-get install -y tesseract-ocr \
  && apt-get install -y libtesseract-dev \
  && apt-get install -y libleptonica-dev \
  && ldconfig \
  && apt install -y libsm6 libxext6 \
  && apt install -y python-opencv \
  && python api.py