FROM node:12

ENV APP_HOME /app

RUN mkdir $APP_HOME

WORKDIR $APP_HOME

CMD echo BUILDING... \
  && cd api-dev/ \
  && npm install  \
  && echo DONE!!!